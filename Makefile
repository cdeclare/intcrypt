CC = g++
CC_FLAGS = -fPIC -std=c++11 -Wall -O2 -I/usr/local/ssl/include -Ipackage -Ipackage/boost
LD_LIBS = -lcryptopp -lboost_system -lboost_regex -lssl -lcrypto -Llib


EXEC = libintcrypt.so
SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:.cpp=.o)

# Main target
$(EXEC): $(OBJECTS)
	$(CC) -shared $(OBJECTS) -o $(EXEC) $(LD_LIBS)

%.o: %.cpp
	$(CC) -c -Wl,-soname,libintcrypt.so.0 $(CC_FLAGS) $< -o $@

clean:
	rm -f $(EXEC) $(OBJECTS)

install:
	cp -f $(EXEC) lib/
	mkdir -p /usr/local/intcrypt/
	cp -f lib/*.so /usr/local/intcrypt/




